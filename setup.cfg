# -*- coding: utf-8 -*-
#
# setup.cfg
#
# Copyright (C) 2022-2023 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of md-toc.
#
# md-toc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# md-toc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with md-toc.  If not, see <http://www.gnu.org/licenses/>.
#

[metadata]
name = md_toc
# 'version' needs setuptools >= 39.2.0.
version = 8.2.0
license = GPLv3+,
description = Automatically generate and add an accurate table of contents to markdown files
long_description=file: README.md
long_description_content_type = text/markdown
author = Franco Masotti
author_email = franco.masotti@tutanota.com
keywords=
    markdown
    toc
    text
    table-of-contents
    documentation
url = https://blog.franco.net.eu.org/software/#md-toc
classifiers=
    Development Status :: 5 - Production/Stable
    Topic :: Utilities
    Topic :: Text Processing :: Markup :: Markdown
    Topic :: Documentation
    Intended Audience :: End Users/Desktop
    Environment :: Console
    License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
    Programming Language :: Python :: 3

[options]
python_requires = >=3.5, <4
install_requires=
    fpyutils >=3.0.1, <4
    importlib-metadata >= 1.0 ; python_version < "3.8"
packages=find:

[options.entry_points]
console_scripts =
    md_toc = md_toc.__main__:main

[options.packages.find]
exclude=
    *tests*

[options.package_data]
* = *.txt, *.rst

[yapf]
based_on_style = pep8
indent_width = 4

[flake8]
ignore =
    E125
    E131
    E501
    W503
    W504
    F401

[isort]
# See
# https://github.com/ESMValGroup/ESMValCore/issues/777
multi_line_output = 3
include_trailing_comma = true
